function MatchesWon(matches) {
    let teams = [...new Set(matches.map((item) => item.team1 && item.team2))]
    // let years = [...new Set(matches.map((item) => item.season))].sort()
    // console.log(teams)
    // console.log(years)


    let count = 0;
    let result = []
    let temp = {}
    for (let i = 0; i < teams.length; i++) {
        for (let j = 0; j < matches.length; j++) {
            if (matches[j]["winner"] == teams[i]) {
                count++;
            }
        }
        temp[teams[i]] = count
        // temp["y"] = count 
        // result.push(temp)
        // temp={}
        count = 0
    }
    // console.log(temp)
    return temp
    // console.log(result)
}

module.exports = MatchesWon;
