const fs = require("fs");
const csv = require("csvtojson");
const matchesPlayedPerYear = require("./ipl/matchesPlayedPerYear");
const matchesWon = require("./ipl/matchesWon");

const MATCHES_FILE_PATH = "./csv_data/matches.csv";
const DELIVERIES_FILE_PATH = "./csv_data/deliveries.csv"
const JSON_OUTPUT_FILE_PATH = "./public/data.json";

function main() {
  csv()
    .fromFile(MATCHES_FILE_PATH)
    .then((matches) => {
      let result = matchesPlayedPerYear(matches);
      let MatchesWon = matchesWon(matches);
      saveMatchesPlayedPerYear(result, MatchesWon);
      // saveMatchesWon(MatchesWon);
    });

  csv()
    .fromFile(DELIVERIES_FILE_PATH)
    .then((deliveries) => {
      // console.log(deliveries.slice(1000));
    })
}

function saveMatchesPlayedPerYear(result, MatchesWon) {
  const jsonData = {
    matchesPlayedPerYear: result,
    matchesWon: MatchesWon
  };
  const jsonString = JSON.stringify(jsonData);
  fs.writeFile(JSON_OUTPUT_FILE_PATH, jsonString, "utf8", err => {
    if (err) {
      console.error(err);
    }
  });
}

main();
